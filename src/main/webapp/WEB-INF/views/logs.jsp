<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">
    <title>统一日志监控平台</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style>
        #msg{
            height: 600px; overflow-y: scroll; background: #333; color: #aaa; padding: 10px;font-size: smaller;
        }
    </style>
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
</head>
<body>
<button onclick="location.reload()">开启日志</button>
<button onclick="closeWebSocket()">关闭日志</button>
<div id="msg" ></div>
<script>
    $(document).ready(function() {
        /* !window.WebSocket、window.MozWebSocket检测浏览器对websocket的支持*/
        if (!window.WebSocket) {
            if (window.MozWebSocket) {
                window.WebSocket = window.MozWebSocket;
            } else {
                $('#msg').prepend("<p>你的浏览器不支持websocket</p>");
            }
        }
        /* ws = new WebSocket 创建WebSocket的实例  注意设置对以下的websocket的地址哦*/
        var wsUrl = '${wsUrl}';
        //var wsUrl = 'ws://logs.efunbox.cn/ws/logs/${uuid}'
        ws = new WebSocket(wsUrl);
        //ws = new WebSocket('ws://172.24.5.204:8888/WebsocketDemo/websocket');
        /*
         ws.onopen  握手完成并创建TCP/IP通道，当浏览器和WebSocketServer连接成功后，会触发onopen消息
         ws.onmessage 接收到WebSocketServer发送过来的数据时，就会触发onmessage消息，参数evt中包含server传输过来的数据;
         */
        ws.onopen = function(evt) {
            $('#msg').append('<li>websocket连接成功</li>');
        }
        ws.onclose = function(evt) {
            $('#msg').append('<li>websocket连接关闭</li>');
        }
        ws.onerror = function(evt) {
            console.info(evt)
            $('#msg').append('<li>websocket连接关闭</li>');
        }
        ws.onmessage = function(evt) {
            //$('#msg').prepend('<li>' + evt.data + '</li>');
            $('#msg').append('<li>' + evt.data + '</li>');
            //滚动条滚到底部，实现日志滚动效果
            $('#msg').scrollTop( $('#msg')[0].scrollHeight );
        }
    });
    //关闭连接
    function closeWebSocket(){
        ws.close();
    }
</script>
</body>
</html>