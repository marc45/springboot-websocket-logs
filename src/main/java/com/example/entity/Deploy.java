package com.example.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "deploy")
public class Deploy {
    @Id
    private String uuid;
    private String name;
    private String env; //枚举:Dev Test Aliyun
    private String user;
    private String host;
    private String logPath;
    private Integer sshPort;
}